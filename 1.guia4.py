#Nombre: edad (Aquí definimos nuestro diccionario inicial)
#Quise usar nombre y edades, en vez de proteinas y especificaciones (Por tema de sencillez)
diccionario = {"José": 19,
               "Andrés": 20,
               "Felipe": 21}
#Con esto se crea un bucle para preguntarle al usuario qué quiere hacer
menu = True
#Opciones del menú
while menu:
    print("*******************************************")
    print("\nHola, ¿Qué deseas hacer?")
    print("""
          1: Para agregar elementos al diccionario.
          2: Para editar elementos del diccionario.
          3: Consulte por algún elemento.
          4: Para eliminar algún valor.
          5: Para imprimir todo el diccionario.\n
          6: Exit\n""")
    print("*******************************************")

    opcion = ""
    while opcion not in ("1","2","3","4","5"):
        opcion = input("->")
#Se pregunta si la opción es 1, si es así...
#se le pide al usuario que agregue otra palabra
    if opcion == "1":
        nombre = input("Ingrese el nombre que desea agregar: ")
        if nombre in diccionario:
            print("Ese nombre ya existe")
        else:
            edad = int(input("Ingrese la edad de esa persona: "))
            diccionario[nombre] = edad
            print("Persona añadida correctamente")
#Se hace lo mismo con el paso anterior pero correspondiente a la opción
    elif opcion == "2":
        nombre = input("Ingrese el nombre de la persona que desea editar: ")
        if nombre not in diccionario: #Aquí se consulta si el nombre existe
            print("Esa persona no existe")
        else:
            edad = int(input("¿Cuántos años tiene esa persona?: "))
            diccionario[nombre] = edad
            print("Edad actualizada correctamente")
#Aquí permite comprobar si existe la persona en el diccionario
    elif opcion == "3":
        nombre = input("Ingrese el nombre de la persona que busca: ")
        if nombre not in diccionario:
            print("Ese nombre no existe en el diccionario")
        else:
            edad = diccionario[nombre]
            print(nombre, ":", edad)
#Aquí podemos borrar una persona del diccionario
    elif opcion == "4":
        nombre = input("Ingrese el nombre de la persona que desea borrar: ")
        if nombre not in diccionario:
            print("No existe ese nombre en el diccionario")
        else:
            del diccionario[nombre]
            print("La persona ha sido eliminada del diccionario")
#Con esto se imprime el diccionario
    elif opcion == "5":
        print(diccionario)
#Aquí el while deja de ser verdad y para de ejecutarse
    elif opcion == "6":
        menu = False
