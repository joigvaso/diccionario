#español-inglés (Aquí definimos nuestro diccionario inicial)
diccionario = {"Hola;": "Hi",
               "Chao;": "Bye",
               "World;": "Mundo"}
#Con esto se crea un bucle para preguntarle al usuario qué quiere hacer
menu = True
#Opciones del menú
while menu:
    print("*******************************************")
    print("\nHola, ¿Qué deseas hacer?")
    print("""
          1: Para agregar otra palabra al diccionario.
          2: Consulte por alguna palabra.
          3: Para imprimir todo el diccionario.\n
          4: Traduzca alguna frase.
          5: Exit\n""")
    print("*******************************************")

    opcion = ""
    while opcion not in ("1","2","3","4","5"):
        opcion = input("->")
#Se pregunta si la opción es 1, si es así...
#se le pide al usuario que agregue otra palabra
    if opcion == "1":
        palabra = input("Ingrese una palabra en español: ")
        if palabra in diccionario:
            print("Esa palabra ya existe.")
        else:
            traduccion = (input("Ingrese la traduccion en inglés: "))
            diccionario[palabra] = traduccion
            print("Palabra agregada correctamente")
#Aquí permite comprobar si existe la palabra en el diccionario
    elif opcion == "2":
        palabra = input("Ingrese la palabra que busca: ")
        if palabra not in diccionario:
            print("Esa palabra no existe en el diccionario.")
        else:
            palabra = diccionario[traduccion]
            print(palabra, ":", traduccion)

#Con esto se imprime el diccionario
    elif opcion == "3":
        print(diccionario)

    elif opcion == "4":
        frase = (input("Ingrese una frase en español: "))
        print("El traductor anda malo hoy, pruebe con DeepL :D!")
#Aquí el while deja de ser verdad y para de ejecutarse
    elif opcion == "5":
        menu = False
